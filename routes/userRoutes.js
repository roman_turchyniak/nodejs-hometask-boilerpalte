const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

// TODO: Implement route controllers for user

router.get("/", (req, res, next) => {
  if (res.statusValue === 400) {
    next();
  }

  const data = UserService.showAll();
  if (!data) {
    res.status = 404;
    res.statusMessage = "Users not exists yet."
    next();
  }
  res.status = 200;
  res.statusMessage = "Here we go."
  const { ...shiftedData } = data;
  res.data = shiftedData;
  next(); 
});

router.get(`/:id`, (req, res, next) => {
  if (res.statusValue === 400) {
    next();
  }
  const data = UserService.search(req.params);
  if (!data) {
    res.statusValue = 404;
    res.statusMessage = "Ooops. We can't find this user"
    next();
  }
  res.statusValue = 200;
  res.statusMessage = "You're welcome."
  res.data = data;
  next();
});

router.post("/", createUserValid, (req, res, next) => {
  if (res.statusValue === 400) {
    next();
    return;
  }
  const data = UserService.createNewUser(req.body);
  res.statusValue = data.status;
  res.statusMessage = data.message;
  res.data = data.result;
  next();
});

router.put("/:id", updateUserValid, (req, res, next) => {
  if (res.statusValue === 400) {
    next();
    return;
  }
  
  const data = UserService.updateUser(req.params, req.body);
  res.statusValue = data.status;
  res.statusMessage = data.message;
  //const { id, ...shiftedData } = data.result;
  res.data = shiftedData;
  next();
});

router.delete("/:id", (req, res, next) => {
  if (res.statusValue === 400) {
    next();
    return;
  }
  const data = UserService.deleteUser(req.params);

  res.statusValue = data.status;
  res.statusMessage = data.message;
  const { id, ...shiftedData } = data.result;
  res.data = shiftedData;

  next();
});

router.use("/", responseMiddleware);

module.exports = router;
