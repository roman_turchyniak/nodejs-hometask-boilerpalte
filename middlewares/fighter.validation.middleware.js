const { fighter } = require("../models/fighter");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  const { name, health, power, defense } = req.body;
  const isIdExists = req.body.hasOwnProperty("id");

  switch (true) {
    case isIdExists:
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body.";
      break;
    case propertiesCheck(req.body, "name"):
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body. Property firstName absent";
      break;
    case isNameInValid(name):
      res.statusValue = 400;
      res.statusMessage = "First name is empty. Please fill Field";
      break;
    case isHealthRangeInValid(health):
      res.statusValue = 400;
      res.statusMessage = "Wrong value for health.";
      break;
    case isPowerRangeInValid(power):
      res.statusValue = 400;
      res.statusMessage = "Wrong value for power.";
      break;
    case isDefenceRangeInValid(defense):
      res.statusValue = 400;
      res.statusMessage = "Wrong value for defense.";
      break;
    default:
      next();
  }
  next();
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  const { name, health, power, defense } = req.body;
  const isIdExists = req.body.hasOwnProperty("id");

  switch (true) {
    case isIdExists:
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body.";
      break;
    case propertiesCheck(req.body, "name"):
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body. Property firstName absent";
      break;
    case isNameInValid(name):
      res.statusValue = 400;
      res.statusMessage = "First name is empty. Please fill Field";
      break;
    case isHealthRangeInValid(health):
      res.statusValue = 400;
      console.log(res.status);
      break;
    case isPowerRangeInValid(power):
      res.statusValue = 400;
      break;
    case isDefenceRangeInValid(defense):
      res.statusValue = 400;
      break;
    default:
      next();
  }
  next();
};

function isHealthRangeInValid(health) {
  if (
    typeof health === "number" &&
    health > 0 &&
    health <= 100 &&
    health.toString().trim() !== ""
  ) {
    return false;
  } else {
    return true;
  }
}

function isPowerRangeInValid(power) {
  if (
    typeof power === "number" &&
    power > 0 &&
    power <= 100 &&
    power.toString().trim() !== ""
  ) {
    return false;
  } else {
    return true;
  }
}

function isDefenceRangeInValid(defense) {
  if (
    typeof defense === "number" &&
    defense > 1 &&
    defense <= 10 &&
    defense.toString().trim() !== ""
  ) {
    return false;
  } else {
    return true;
  }
}

function isNameInValid(name) {
  if (name.toString().trim() === "") {
    return true;
  } else {
    return false;
  }
}

function propertiesCheck(obj, prop) {
  let resObj = Object.keys(obj).sort();
  let state = true;

  resObj.forEach((element) => {
    if (element === prop) {
      state = false;
    }
  });

  return state;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
