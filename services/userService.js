const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  showAll() {
    let itemList = UserRepository.getAll();
    // let filteredList = [];
    // itemList.forEach((element) => {
    //   const { id, ...filteredItem } = element;
    //   filteredList.push(filteredItem);
    // });
    return itemList;
  }

  createNewUser(data) {
    let toBeCreated = UserRepository.getOne(data);
    let status, message;
    let user;

    if (!toBeCreated) {
      user = UserRepository.create(data);
      message = "User created successfuly ";
      status = 200;
    } else {
      message = "User already exist. Try remember your login parameters.";
      status = 400;
    }

    return (data = { message: message, status: status, result: user });
  }

  updateUser(id, data) {
    let toBeUpdated = UserRepository.getOne(id);
    let status, message;

    if (toBeUpdated) {
      UserRepository.update(id.id, data);
      message = "User updated successfuly";
      status = 200;
    } else {
      message = "User not found. Check parameters";
      status = 404;
    }

    return (data = { message: message, status: status });
  }

  deleteUser(data) {
    let toBeDeleted = UserRepository.getOne(data);
    let status, message;

    if (toBeDeleted) {
      UserRepository.delete(data.id);
      message = "User deleted successfuly";
      status = 200;
    } else {
      message = "User not found. Check parameters";
      status = 404;
    }

    return (data = { message: message, status: status });
  }
}

module.exports = new UserService();
