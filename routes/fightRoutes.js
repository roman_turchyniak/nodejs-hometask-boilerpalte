const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights

router.get('/', (req, res, next) => {
    
})

router.post("/", (req, res, next) => {   
    const data = FightService.fight(req.param.fighter1, req.param.fighter2);
    next();
  });

module.exports = router;