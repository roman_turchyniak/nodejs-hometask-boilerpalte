const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    // const {id, ...filteredItem} = item
    return item;
  }

  showAll() {
    let itemList = FighterRepository.getAll();
    // let filteredList = [];
    // itemList.forEach(element => { const { id, ...filteredItem} = element
    //   filteredList.push(filteredItem);
    // });
    return itemList;
  }

  createNewFighter(data) {
    let toBeCreated = FighterRepository.getOne(data);
    let status, message;
    let fighter;

    if (!toBeCreated) {
      fighter = FighterRepository.create(data);
      message = "Fighter created successfuly ";
      status = 200;
    } else {
      message = "Fighter already exist.";
      status = 400;
    }
    
    return (data = { message: message, status: status, result: fighter });
  }

  updateFighter(id, data) {
    let toBeUpdated = FighterRepository.getOne(id);
    let status, message;
    

    if (toBeUpdated) {
      FighterRepository.update(id.id, data);
      message = "Fighter updated successfuly.";
      status = 200;
    } else {
      message = "Fighter not found.";
      status = 404;
    }
    
    return data = {message: message, status: status};
  }

  deleteFighter(data) {
    let toBeDeleted = FighterRepository.getOne(data);
    let status, message;

    if (toBeDeleted) {
      FighterRepository.delete(data.id);
      message = "Fighter deleted successfuly";
      status = 200;
    } else {
      message = "Fighter not found."
      status = 404;
    }

    return data = {message: message, status: status};
  }
}

module.exports = new FighterService();
