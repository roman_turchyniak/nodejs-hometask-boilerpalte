const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get("/", (req, res, next) => {
  if (res.statusValue === 400) {
    next();
  }

  const data = FighterService.showAll();
  if (!data) {
    res.status = 404;
    res.statusMessage = "Fighters not exists yet."
    next();
  }
  res.status = 200;
  res.statusMessage = "Here we go."   
  res.data = data;
  next(); 
});

router.get(`/:id`, (req, res, next) => {
  if (res.statusValue === 400) {
    next();
  }
  const data = FighterService.search(req.params);
  if (!data) {
    res.statusValue = 404;

    next();
  }
  res.statusValue = 200;
  res.data = data;
  next();
});

router.post("/", createFighterValid, (req, res, next) => {
  if (res.statusValue === 400) {
    next();
    return;
  }
  const data = FighterService.createNewFighter(req.body);
  res.statusValue = data.status;
  res.statusMessage = data.message;
  res.data = data.result;
  next();
});

router.put("/:id", updateFighterValid, (req, res, next) => {
  if (res.statusValue === 400) {
    next();
    return;
  }
  
  const data = FighterService.updateFighter(req.params, req.body);
  res.statusValue = data.status;
  res.statusMessage = data.message;
  //const { id, ...shiftedData } = ;
  res.data = data.result;
  next();
});

router.delete("/:id", (req, res, next) => {
  if (res.statusValue === 400) {
    next();
    return;
  }
  const data = UserService.deleteUser(req.params);

  res.statusValue = data.status;
  res.statusMessage = data.message;
  const { id, ...shiftedData } = data.result;
  res.data = shiftedData;

  next();
});

router.use("/", responseMiddleware);

module.exports = router;