const { user } = require("../models/user");
const { responseMiddleware } = require("../middlewares/response.middleware");
const UserService = require("../services/userService");

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  const { firstName, lastName, email, phoneNumber, password } = req.body;
  const isIdExists = req.body.hasOwnProperty("id");
  propertiesCheck(req.body);

  switch (true) {
    case isIdExists:
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body.";
      break;
    case propertiesCheck(req.body, "firstName"):
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body. Property firstName absent";
      break;
    case propertiesCheck(req.body, "lastName"):
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body. Property lastName absent";
      break;
    case propertiesCheck(req.body, "email"):
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body. Property email absent";
      break;
      case propertiesCheck(req.body, "phoneNumber"):
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body. Property phone number absent";
      break;
      case propertiesCheck(req.body, "password"):
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body. Property password absent";
      break;
    case isNameInValid(firstName):
      res.statusValue = 400;
      res.statusMessage = "First name is empty. Please fill Field";
      break;
    case isNameInValid(lastName):
      res.statusValue = 400;
      res.statusMessage = "Last name is empty. Please fill Field";
      break;
    case isPasswordInValid(password):
      res.statusValue = 400;
      res.statusMessage = "Password is too short";
      break;
    case isEmailInValid(email):
      res.statusValue = 400;
      res.statusMessage = "email is not valid or already used.";
      break;
    case isPhoneNumberInValid(phoneNumber):
      res.statusValue = 400;
      res.statusMessage = "Phone number is not valid or already used.";
      break;
    default:
      next();
  }
  next();
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  const { firstName, lastName, email, phoneNumber, password } = req.body;

  const isIdExists = req.body.hasOwnProperty("id");

  switch (true) {
    case isIdExists:
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body.";
      break;
    case propertiesCheck(req.body, "firstName"):
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body. Property firstName absent";
      break;
    case propertiesCheck(req.body, "lastName"):
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body. Property lastName absent";
      break;
    case propertiesCheck(req.body, "email"):
      res.statusValue = 400;
      res.statusMessage = "Wrong format of response body. Property email absent";
      break;
    case isNameInValid(firstName):
      res.statusValue = 400;
      res.statusMessage = "First name is empty. Please fill Field";
      break;
    case isNameInValid(lastName):
      res.statusValue = 400;
      res.statusMessage = "Last name is empty. Please fill Field";
      break;
    case isPasswordInValid(password):
      res.statusValue = 400;
      res.statusMessage = "Password is too short";
      break;
    case isEmailInValid(email):
      res.statusValue = 400;
      res.statusMessage = "email is not valid or already used.";
      break;
    case isPhoneNumberInValid(phoneNumber):
      res.statusValue = 400;
      res.statusMessage = "Phone number is not valid or already used.";
      break;
    default:
      next();
  }
  next();
};

function isPasswordInValid(password) {
  if (password.length > 3) {
    return false;
  } else {
    return true;
  }
}
function isEmailInValid(email) {
  let isEmailExists;
  let mailCheck = { email: `${email}` };
  const mailFromDb = UserService.search(mailCheck);

  if (mailFromDb === null) {
    isEmailExists = false;
  } else {
    isEmailExists = true;
  }

  if (
    email.toString().trim() !== "" &&
    email.length > 10 &&
    email.trim().substring(email.length - 10, email.length) === "@gmail.com" &&
    !isEmailExists
  ) {
    return false;
  } else {
    return true;
  }
}
function isPhoneNumberInValid(phoneNumber) {
  let isphoneNumberExists;
  let phoneNumberCheck = { phoneNumber: `${phoneNumber}` };
  const phoneNumberFromDb = UserService.search(phoneNumberCheck);

  if (phoneNumberFromDb === null) {
    isphoneNumberExists = false;
  } else {
    isphoneNumberExists = true;
  }

  if (
    phoneNumber.toString().trim() !== "" &&
    phoneNumber.length === 13 &&
    phoneNumber.trim().substring(0, 4) === "+380" &&
    !isphoneNumberExists
  ) {
    return false;
  } else {
    return true;
  }
}

function isNameInValid(name) {
  if (name.toString().trim() === "") {
    return true;
  } else {
    return false;
  }
}

function propertiesCheck(obj, prop) {
  let resObj = Object.keys(obj).sort();
  let state = true;

  resObj.forEach((element) => {
    if (element === prop) {
      state = false;
    }
  });

  return state;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
