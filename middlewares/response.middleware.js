const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query

  switch (true) {    
      case res.statusValue === 400:
        body = {
          error: true,
          message: `Invalid input or problems with processing data. ${res.statusMessage}`
        };
        res.status(400).send(body);
        break;
      case res.statusValue === 404:
        body = {
          error: true,
          message: `Data not found. ${res.statusMessage}`
        };
        res.status(404).send(body);
        break;
        default:
          if(res.data){
            body = res.data;
          } else {body = {error: false, message: res.statusMessage};}
          res.send(body);
          return;
  }
};

exports.responseMiddleware = responseMiddleware;
